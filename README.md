# What is this project?

This is a schoolwork project developed in Software Engineering course in the University of Tartu.

**Authors**: Robert Joonas, Max Frolov, Kristo M�nna

This Point-of-Sale application is developed on a given [template](https://bitbucket.org/MTAT-03-094/pos2020/src/master/)

In this project, we went through a mock-up software development process.
At the beginning, we did some requirements gathering, set some goals and tasks.
Then, homework by homework we started implementing different functionalities, finally reaching [this finishing result](https://bitbucket.org/RobertJoonas/lab10-rkm/src/master/).

There are two applications in this project: **SalesSystemCLI (command-line version)** and **SalesSystemGUI (graphical user interface version)**

Both applications have two different "data-access-objects" to run with:

1.  **InMemorySalesSystemDAO** - no data persistence (default)
2.  **HibernateSalesSystemDAO** - data persistence

To run with Hibernate, use **gradlew hsql** command to start the database, before starting the app.

Ways to actually start the app:

1.  run the **SaleSystemGUI:run** (or **SaleSystemCLI:run**) gradle task
2.  download the distribution zip file, extract it, run the .bat file in SaleSystemGUI-1.0/bin folder (on Windows) - [ZIP](https://drive.google.com/file/d/1_qtzppNFo4FIKQBroAtYrDaqfGZqzKuS/view?usp=sharing)

**Second way of running only works with InMemorySalesSystemDAO**

# Our experience

Before starting this course, we all had experience with Java.
During this progress, we used BitBucket to manage our project (also to learn project management via Git).
With the actual development process, we got some useful knowledge about working with Gradle, JavaFX, 
and later also some introduction to databases and unit-testing with Hibernate ORM and JUnit. The overall handling 
of a Gradle/Java project was definitely a valueable experience for us all.


# Schoolwork:

## Team <lab10-RKM>:
1. <Robert Joonas>
2. <Max Frolov>
3. <Kristo M�nna>

## Homework 1:
[Link](https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/HW1)

## Homework 2:
[Link](https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/HW2)

[TASKS](https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/TASKS)

## Homework 3:
<Links to the solution>

## Homework 4:
<Links to the solution>

## Homework 5:
[Link to RMK wiki page](https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/browse/)

## Homework 6:
<Links to the solution>
[Link] (https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/HW6)

## Homework 7:
<Links to the solution>
[Link] (https://bitbucket.org/RobertJoonas/lab10-rkm/wiki/HW7)

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)