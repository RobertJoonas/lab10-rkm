package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.dataobjects.TeamData;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final TeamData teamdata;
    private final Warehouse warehouse;

    public ConsoleUI() throws IOException {
        this.dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);
        warehouse = new Warehouse(dao);
        teamdata = new TeamData();
    }

    public static void main(String[] args) throws Exception {
        ConsoleUI console = new ConsoleUI();
        log.debug("Running ConsoleUI");
        console.run();
    }

    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.findStockItems();
        log.info("Showing stock items:");
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si);
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        log.info("Showing cart contents:");
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si);
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showHistory(LocalDateTime fromDate, LocalDateTime toDate) {

        List<HistoryItem> entries = dao.findAllHistoryItems();
        log.info("Showing history from " + fromDate + " to " + toDate);
        System.out.println("-------------------------");
        for (HistoryItem h : entries) {
            if (h.getDate().isAfter(fromDate) && h.getDate().isBefore(toDate)) {
                printHistoryItem(h);
            }
        }
        if (entries.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void ShowAllHistory() {

        List<HistoryItem> entries = dao.findAllHistoryItems();
        log.info("Showing all history:");
        System.out.println("-------------------------");
        for (HistoryItem h : entries) {
            printHistoryItem(h);
        }
        if (entries.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void printHistoryItem(HistoryItem h) {
        String d = h.getDate().toString();
        System.out.println("Date: " + d.substring(0, d.indexOf("T")) + "\tTotal price: " + h.getTotal());
        for (SoldItem s : h.getSoldItems()) {
            System.out.println("\t" + s);
        }
    }

    private void ShowLast10Days() {
        LocalDateTime startDate = LocalDateTime.now().minusDays(10);
        LocalDateTime endDate = LocalDateTime.now();
        showHistory(startDate, endDate);
    }

    private void ShowBetweenDates(String start, String end) {
        LocalDateTime startDate = stringToLocalDateTime(start, "00:00:00");
        LocalDateTime endDate = stringToLocalDateTime(end, "23:59:59");
        showHistory(startDate, endDate);
    }

    private LocalDateTime stringToLocalDateTime(String dateString, String timeString) {
        DateTimeFormatter f = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate d = LocalDate.parse(dateString, f);
        String[] arr = timeString.split(":");
        int hour = Integer.parseInt(arr[0]);
        int minute = Integer.parseInt(arr[1]);
        int second = Integer.parseInt(arr[2]);
        return LocalDateTime.of(d, LocalTime.of(hour, minute, second));
    }


    private void showTeam() {
        System.out.println("-------------------------");

        System.out.println("Team name: " + teamdata.getTeamName());
        System.out.println("Team leader: " + teamdata.getTeamLeader());
        System.out.println("Team leader email: " + teamdata.getTeamLeaderEmail());
        System.out.println("Team members: " + teamdata.getTeamMembers());

        System.out.println("-------------------------");

    }

    private void AddItemToCart(long index, int amount) {
        StockItem item = dao.findStockItem(index);
        if (item != null) {
            cart.addItem(new SoldItem(item, amount));
            log.debug("Added item to the shopping cart: " + item);
        } else {
            System.out.println("no stock item with id " + index);
        }
    }

    private void AddItemToWarehouse(long barCode, String name, double price, int amount) {
        warehouse.addToStock(barCode, name, "", price, amount);
        log.debug("Added " + amount + " items[" + name + "] to warehouse: ");
        //Show stock again
        showStock();
    }

    private void RemoveItemFromCart(long index) {
        cart.removeItem(index);
        showCart();
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("c\t\tShow cart contents");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("t\t\tShow Team info");
        System.out.println("sa Barcode Amount \t\t\t\tAdd Amount of stock item with Index to the cart");
        System.out.println("sr Index  \t\t\t\t\t\tRemove item at index from shopping cart");
        System.out.println("wa Barcode Name Price Amount \tAdd new or increase amount of existing stock to warehouse");
        System.out.println("hb from to \t\t\t\t\t\tShow history between from and to dates. (insert dates as: dd/mm/yyyy)");
        System.out.println("ht \t\t\t\t\t\t\t\tShow last 10 days history");
        System.out.println("ha \t\t\t\t\t\t\t\tShow all history");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");
        try {
            if (c[0].equals("h"))
                printUsage();
            else if (c[0].equals("q"))
                System.exit(0);
            else if (c[0].equals("w"))
                showStock();
            else if (c[0].equals("c"))
                showCart();
            else if (c[0].equals("t"))
                showTeam();
            else if (c[0].equals("p"))
                cart.submitCurrentPurchase();
            else if (c[0].equals("r"))
                cart.cancelCurrentPurchase();
            else if (c[0].equals("sa") && c.length == 3)
                AddItemToCart(Long.parseLong(c[1]), Integer.parseInt(c[2]));
            else if (c[0].equals("sr") && c.length == 2)
                RemoveItemFromCart(Long.parseLong(c[1]));
            else if (c[0].equals("wa") && c.length == 5)
                AddItemToWarehouse(Long.parseLong(c[1]), c[2], Double.parseDouble(c[3]), Integer.parseInt(c[4]));
            else if (c[0].equals("hb") && c.length == 3)
                ShowBetweenDates(c[1], c[2]);
            else if (c[0].equals("ht"))
                ShowLast10Days();
            else if (c[0].equals("ha"))
                ShowAllHistory();
            else
                System.out.println("unknown command");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
