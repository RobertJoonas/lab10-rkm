package ee.ut.math.tvt.salessystem.ui;

import java.io.IOException;

public class Launcher {
    public static void main(String[] args) throws IOException {
        new SalesSystemUI().main(args);
    }
}
