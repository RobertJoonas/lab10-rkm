package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.*;
import ee.ut.math.tvt.salessystem.logic.History;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class HistoryController implements Initializable {

    private SalesSystemDAO dao;
    private History history;
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    @FXML
    private Button showBetweenDates;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private Button showLast10;
    @FXML
    private Button showAll;
    @FXML
    private TableView<HistoryItem> historyTableView;
    @FXML
    private TableView<SoldItem> soldItemsView;


    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
        this.history = new History(dao);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        historyTableView.setItems(FXCollections.observableList(dao.findAllHistoryItems()));
        ChangeListener cl = new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                HistoryItem h = historyTableView.getSelectionModel().getSelectedItem();
                if (h != null) {
                    soldItemsView.setItems(FXCollections.observableList(h.getSoldItems()));
                }
            }
        };
        historyTableView.getSelectionModel().selectedItemProperty().addListener(cl);
    }

    @FXML
    protected void showBetweenDatesButtonClicked() {
        try {
            LocalDateTime start = startDate.getValue().atStartOfDay();
            LocalDateTime end = endDate.getValue().atTime(23, 59);
            if (start.isAfter(end)) throw new IllegalArgumentException("End date has to be after start date");
            historyTableView.setItems(FXCollections.observableList(dao.findBetweenDates(start, end)));
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Both dates need to be defined!");
        } catch (IllegalArgumentException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @FXML
    protected void showLast10ButtonClicked() {
        historyTableView.setItems(FXCollections.observableList(dao.findLast10()));
    }

    @FXML
    protected void showAllButtonClicked() {
        historyTableView.setItems(FXCollections.observableList(dao.findAllHistoryItems()));
    }

}

