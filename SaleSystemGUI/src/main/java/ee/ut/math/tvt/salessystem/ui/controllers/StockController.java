package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);
    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    @FXML
    private Button addButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button removeButton;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField amountField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
        this.warehouse = new Warehouse(dao);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    @FXML
    public void addButtonClicked() {
        try {
            long id = Long.parseLong(barCodeField.getText());
            int quantity = Integer.parseInt(amountField.getText());
            String name = nameField.getText();
            double price = Double.parseDouble(priceField.getText());

            warehouse.addToStock(id, name, "", price, quantity);
            refreshStockItems();

        } catch (NumberFormatException e) {
            log.error("Error with reading data. Id, quantity or price value could not be converted");
            JOptionPane.showMessageDialog(null, "NumberFormatException. Check the input fields!");
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @FXML
    public void removeButtonClicked() {
        StockItem selected = warehouseTableView.getSelectionModel().getSelectedItem();
        long thisID = selected.getId();
        if (selected != null) {
            dao.beginTransaction();
            //get rid of the references to that StockItem, before deleting it from the database
            for (HistoryItem h : dao.findAllHistoryItems()) {
                h.loseStockItemReferences(thisID);
            }
            dao.removeFromStock(thisID);
            dao.commitTransaction();
        }
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.findStockItems()));
        warehouseTableView.refresh();
        log.debug("Warehouse refreshed. " + warehouseTableView.getItems().size() + " rows in the table");
    }

    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem == null) {
            resetProductField();
            return;
        }
        nameField.setText(stockItem.getName());
        priceField.setText(String.valueOf(stockItem.getPrice()));
        amountField.setText("1");
    }

    private void resetProductField() {
        amountField.setText("1");
        nameField.setText("");
        priceField.setText("");
    }
}
