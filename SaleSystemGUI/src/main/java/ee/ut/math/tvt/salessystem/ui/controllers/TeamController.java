package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dataobjects.TeamData;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);
    private final TeamData teamData;

    @FXML
    private Label name;

    @FXML
    private Label members;

    @FXML
    private Label leader;

    @FXML
    private Label email;

    @FXML
    private Image logo;

    @FXML
    private ImageView logo2;

    public TeamController(TeamData teamData) {
        this.teamData = teamData;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //display the data from TeamData
        log.debug("loading team tab data.");
        name.setText("Team name: " + teamData.getTeamName());
        members.setText("Team members: " + teamData.getTeamMembers());
        leader.setText("Team leader: " + teamData.getTeamLeader());
        email.setText("Team Leader Email: " + teamData.getTeamLeaderEmail());
        logo2.setImage(new Image(String.valueOf(teamData.getTeamPicture())));
        log.debug("Team data loaded");
    }
}
