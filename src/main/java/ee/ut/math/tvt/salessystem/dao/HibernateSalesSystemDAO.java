package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDateTime;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    // TODO implement missing methods

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("FROM StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        em.persist(stockItem);
        commitTransaction();
    }

    @Override
    public void removeFromStock(Long id) {
        em.remove(findStockItem(id));
    }

    @Override
    public List<HistoryItem> findAllHistoryItems() {
        return em.createQuery("FROM HistoryItem", HistoryItem.class).getResultList();
    }

    public List<HistoryItem> findLast10() {
        return em.createQuery("FROM HistoryItem AS h ORDER BY h.date DESC", HistoryItem.class).setMaxResults(10).getResultList();
    }

    public List<HistoryItem> findBetweenDates(LocalDateTime start, LocalDateTime endDate) {
        return em.createQuery("FROM HistoryItem AS h WHERE h.date BETWEEN :start AND :endDate", HistoryItem.class)
                .setParameter("start", start).setParameter("endDate", endDate)
                .getResultList();
        //return em.createQuery("FROM HistoryItem", HistoryItem.class).getResultList();
    }

    @Override
    public void saveHistoryItem(HistoryItem item) {
        em.persist(item);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
}
