package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    public final List<StockItem> stockItemList;
    public final List<HistoryItem> historyItemList;
    public List<String> methodsCalled = new ArrayList<>();

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.historyItemList = new ArrayList<>();
    }

    public List<String> getMethodsCalled() {
        return this.methodsCalled;
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        stockItemList.add(stockItem);
        commitTransaction();
    }

    @Override
    public void removeFromStock(Long id) {
        StockItem toBeRemoved = findStockItem(id);
        this.stockItemList.remove(toBeRemoved);
    }

    @Override
    public List<HistoryItem> findAllHistoryItems() {
        return this.historyItemList;
    }

    @Override
    public List<HistoryItem> findLast10() {
        List<HistoryItem> result = findAllHistoryItems();
        if (result.size() <= 10) return result;
        else {
            return result.subList(0, 10);
        }
    }

    @Override
    public List<HistoryItem> findBetweenDates(LocalDateTime start, LocalDateTime end) {
        List<HistoryItem> result = new ArrayList<>();
        for (HistoryItem item : findAllHistoryItems()) {
            LocalDateTime date = item.getDate();
            if (date.isAfter(start) || date.isEqual(start)) {
                if (date.isBefore(end) || date.isEqual(end)) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    @Override
    public void saveHistoryItem(HistoryItem item) {
        historyItemList.add(item);
    }

    @Override
    public void beginTransaction() {
        methodsCalled.add("beginTransaction");
    }

    @Override
    public void rollbackTransaction() {
        methodsCalled.add("rollbackTransaction");
    }

    @Override
    public void commitTransaction() {
        methodsCalled.add("commitTransaction");
    }
}
