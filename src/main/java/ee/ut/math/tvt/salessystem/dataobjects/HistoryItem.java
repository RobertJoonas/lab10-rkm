package ee.ut.math.tvt.salessystem.dataobjects;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "HISTORYITEM")
public class HistoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(cascade = CascadeType.ALL)
    private java.util.List<ee.ut.math.tvt.salessystem.dataobjects.SoldItem> soldItems;

    private java.time.LocalDateTime date;

    public HistoryItem(List<SoldItem> soldItems) {
        this.date = LocalDateTime.now();
        this.soldItems = soldItems;
    }

    public HistoryItem() {

    }

    //This method is used in FXML, do not delete
    public String getDateString() {
        String str = date.toString();
        return str.substring(0, str.indexOf("T"));
    }

    public String getTimeString() {
        String str = date.toString();
        int index = str.indexOf("T");
        return str.substring(index + 1, index + 6);
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    public void addSoldItem(SoldItem item) {
        soldItems.add(item);
    }

    //This method is used in FXML, do not delete
    public double getTotal() {
        double total = 0;
        for (SoldItem item : soldItems) {
            total += item.getTotal();
        }
        return total;
    }

    //Method for getting rid of the references to a StockItem, so that the StockItem could be deleted
    public void loseStockItemReferences(long id) {
        for (SoldItem s : this.soldItems) {
            StockItem si = s.getStockItem();
            if (si != null) {
                if (si.getId() == id) {
                    s.setStockItem(null);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "HistoryItem{" +
                "date=" + date +
                '}';
    }
}
