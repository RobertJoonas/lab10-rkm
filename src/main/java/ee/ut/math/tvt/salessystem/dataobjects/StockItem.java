package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;

/**
 * Stock item.
 */
@Entity
@Table(name = "STOCKITEM")
public class StockItem {

    @Id
    private Long id;

    private String name;
    private double price;
    private String description;
    private int quantityInStock;

    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, double price, int quantityInStock) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public StockItem Clone() {
        return new StockItem(getId(), getName(), getDescription(), getPrice(), getQuantityInStock());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "StockItem{" +
                "id=" + this.id +
                " | name=" + this.name +
                " | price=" + this.price +
                " | QuantityInStock=" + this.quantityInStock +
                "}";
    }
}
