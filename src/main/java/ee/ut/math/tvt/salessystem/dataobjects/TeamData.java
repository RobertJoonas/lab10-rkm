package ee.ut.math.tvt.salessystem.dataobjects;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class TeamData {
    private String TeamLeader;
    private String TeamName;
    private String TeamLeaderEmail;
    private Properties properties;
    private String TeamMembers;
    private URL TeamPicture;

    public TeamData() throws IOException {
        //set the data from file
        setData();
    }

    public String getTeamLeader() {
        return TeamLeader;
    }

    public String getTeamLeaderEmail() {
        return TeamLeaderEmail;
    }

    public String getTeamName() {
        return TeamName;
    }

    public String getTeamMembers() {
        return TeamMembers;
    }

    public URL getTeamPicture() {
        return TeamPicture;
    }

    public void setData() throws IOException {
        properties = new Properties();
        try (InputStream is = TeamData.class.getClassLoader().getResourceAsStream("application.properties")) {
            properties.load(is);
            this.TeamName = properties.getProperty("TeamName");
            this.TeamMembers = properties.getProperty("TeamMembers");
            this.TeamLeader = properties.getProperty("TeamLeader");
            this.TeamLeaderEmail = properties.getProperty("TeamLeaderEmail");
            this.TeamPicture = getClass().getClassLoader().getResource(properties.getProperty("TeamPicture"));
        }
    }
}
