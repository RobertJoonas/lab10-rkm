package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class History {

    private static final Logger log = LogManager.getLogger(History.class);
    private final SalesSystemDAO dao;

    public History(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<HistoryItem> showBetweenDates(LocalDateTime start, LocalDateTime end) {
        if (start == null) throw new SalesSystemException("starting date not defined");
        if (end == null) throw new SalesSystemException("end date not defined");
        if (end.isAfter(start)) {
            List<HistoryItem> result = new ArrayList<>();
            for (HistoryItem item : dao.findAllHistoryItems()) {
                LocalDateTime date = item.getDate();
                if (date.isAfter(start) || date.isEqual(start)) {
                    if (date.isBefore(end) || date.isEqual(end)) {
                        result.add(item);
                    }
                }
            }
            return result;
        } else {
            throw new SalesSystemException("End date can not be before start date!");
        }
    }

    public List<HistoryItem> showLast10() {
        List<HistoryItem> result = this.dao.findAllHistoryItems();
        if (result.size() <= 10) return result;
        else {
            return result.subList(0, 10);
        }
    }

    public List<HistoryItem> showAll() {
        return dao.findAllHistoryItems();
    }

}
