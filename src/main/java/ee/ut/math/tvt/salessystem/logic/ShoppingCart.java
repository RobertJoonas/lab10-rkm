package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;

import java.util.ArrayList;
import java.util.List;

import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        long id = item.getId();
        int quantity = item.getQuantity();

        log.debug("In stock: " + dao.findStockItem(id));
        log.debug("quantity: " + quantity);

        if (!enoughInStock(id, quantity + getItemQuantity(id))) {
            log.error("Not enough " + item.getName() + " in stock!");
            throw new SalesSystemException("Not enough " + item.getName() + " in stock!");
        }

        boolean itemAlreadyExists = false;

        for (SoldItem s : items) {
            if (s.getId() == id) {
                itemAlreadyExists = true;
                //increase the quantity of that item already in the list
                s.setQuantity(s.getQuantity() + quantity);
                break;
            }
        }

        if (!itemAlreadyExists) {
            items.add(item);
        }

        log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    private boolean enoughInStock(long id, int quantity) {
        StockItem item = dao.findStockItem(id);
        if (item.getQuantityInStock() >= quantity) return true;
        return false;
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        log.debug("Purchase cancelled");
        items.clear();
    }

    public double GetTotalPrice() {
        int price = 0;
        for (SoldItem item : items) price += item.getPrice() * item.getQuantity();
        return price;
    }

    public void submitCurrentPurchase() {

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        HistoryItem newHistoryItem = new HistoryItem(new ArrayList<>());
        try {
            for (SoldItem item : items) {
                newHistoryItem.addSoldItem(item);
                //decreasing quantity in stock:
                StockItem stockItem = dao.findStockItem(item.getId());
                stockItem.setQuantityInStock(stockItem.getQuantityInStock() - item.getQuantity());
            }
            //Loop through the warehouse, and see which products are out of stock, so that we can remove them
            ArrayList<Long> removables = new ArrayList<>();
            for (StockItem si : dao.findStockItems()) {
                if (si.getQuantityInStock() == 0) {
                    //get rid of the references to that StockItem, before deleting it from the database
                    for (HistoryItem h : dao.findAllHistoryItems()) {
                        h.loseStockItemReferences(si.getId());
                    }
                    newHistoryItem.loseStockItemReferences(si.getId());
                    removables.add(si.getId());
                }
            }
            for (long id : removables) {
                dao.removeFromStock(id);
            }
            dao.saveHistoryItem(newHistoryItem);
            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            log.error("Could not submit purchase");
            dao.rollbackTransaction();
            throw new SalesSystemException("Could not submit purchase");
        }
    }

    public int getItemQuantity(long id) {
        for (SoldItem item : items) {
            if (id == item.getId()) return item.getQuantity();
        }
        return 0;
    }

    public void removeItem(long id) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == id) {
                items.remove(i);
            }
        }
    }

}
