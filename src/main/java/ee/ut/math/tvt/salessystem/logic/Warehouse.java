package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {

    private static final Logger log = LogManager.getLogger(Warehouse.class);
    private final SalesSystemDAO dao;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addToStock(Long id, String name, String desc, double price, int quantity) {

        //check the arguments
        if (id <= 0) throw new IllegalArgumentException("Id must be greater than 0");
        if (price < 0) throw new IllegalArgumentException("price must be greater than or equal to 0");
        if (quantity <= 0) throw new IllegalArgumentException("Quantity must be greater than 0");

        //check if item with this barcode already exists in the warehouse
        StockItem item = dao.findStockItem(id);

        if (item != null) { //this product is already present in the warehouse
            //increase quantity of that item
            boolean pricesMatch = price == item.getPrice();
            boolean namesMatch = name.equals(item.getName());
            //if those both match, we can increase the quantity of that item, otherwise it's maybe a mistake
            if (namesMatch && pricesMatch) {
                item.setQuantityInStock(item.getQuantityInStock() + quantity);
            } else { //one of those didn't match
                log.error("item with this ID already exists. Nothing was added");
            }
        } else { //item not in the warehouse
            dao.saveStockItem(new StockItem(id, name, desc, price, quantity));
        }
    }
}
