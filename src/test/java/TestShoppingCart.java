import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Before;
import org.junit.Test;
import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.List;

import static org.junit.Assert.*;

public class TestShoppingCart {

    InMemorySalesSystemDAO dao;
    ShoppingCart cart;

    @Before
    public void setUp() {

        this.dao = new InMemorySalesSystemDAO();
        this.cart = new ShoppingCart(dao);

        /** THESE ITEMS ARE ADDED IN CONSTRUCTOR WHEN CREATING THE InMemorySalesSystemDAO
         * items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
         * items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
         * items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
         * items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
         */
    }

    /**
     * SHOPPING CART ADDING TESTS
     */

    @Test (expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLarge() {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        StockItem chips = dao.findStockItem(1);
        cart.addItem(new SoldItem(chips, 6)); //added 6 to cart, should give SalesSystemException
    }

    @Test (expected = SalesSystemException.class)
    public void testAddingItemWithQuantitySumTooLarge() {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        StockItem chips = dao.findStockItem(1);
        cart.addItem(new SoldItem(chips, 3)); //added 3 of those in cart
        cart.addItem(new SoldItem(chips, 3)); //add again, should give SalesSystemException
    }

    /**
     * SUBMITTING PURCHASE TESTS
     */

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        //and StockItem {2L, "Chupa-chups", "Sweets", 8.0, 8} (quantity = 8)
        StockItem chips = dao.findStockItem(1);
        StockItem chups = dao.findStockItem(2);
        int chipsQuantityBefore = chips.getQuantityInStock();
        int chupsQuantityBefore = chups.getQuantityInStock();
        cart.addItem(new SoldItem(chips, 3));
        cart.addItem(new SoldItem(chups, 3));
        cart.submitCurrentPurchase();
        int chipsQuantityAfter = chips.getQuantityInStock();
        int chupsQuantityAfter = chups.getQuantityInStock();
        assertTrue(chipsQuantityAfter < chipsQuantityBefore && chupsQuantityAfter < chupsQuantityBefore);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction () {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        cart.addItem(new SoldItem(dao.findStockItem(1), 3));
        cart.submitCurrentPurchase();
        List<String> calledMethods = dao.getMethodsCalled();
        boolean firstBegin = calledMethods.get(0).equals("beginTransaction");
        boolean thenCommit = calledMethods.get(1).equals("commitTransaction");
        boolean bothOnlyOnce = calledMethods.size() == 2;
        assertTrue(firstBegin && thenCommit && bothOnlyOnce);
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem () {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        int historyItemArrayLengthBefore = dao.findAllHistoryItems().size();
        SoldItem si = new SoldItem(dao.findStockItem(1), 3);
        cart.addItem(si);
        cart.submitCurrentPurchase();
        List<HistoryItem> hil = dao.findAllHistoryItems();
        int historyItemArrayLengthAfter = hil.size();
        HistoryItem lastItem = hil.get(hil.size() - 1);
        List<SoldItem> sil = lastItem.getSoldItems();
        boolean containsSameSoldItems = sil.size() == 1 && sil.get(0).equals(si);
        assertTrue(historyItemArrayLengthBefore == historyItemArrayLengthAfter - 1 && containsSameSoldItems);
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime () {
        //using the StockItem {1L, "Lays chips", "Potato chips", 11.0, 5} (quantity = 5)
        cart.addItem(new SoldItem(dao.findStockItem(1), 3));
        LocalDateTime now = LocalDateTime.now();
        cart.submitCurrentPurchase();
        //Added a historyItem to the end of the historyitem list
        List<HistoryItem> hil = dao.findAllHistoryItems();
        HistoryItem lastItem = hil.get(hil.size() - 1); // got the last historyItem of the list, which is what we want
        int nowSeconds = now.toLocalTime().toSecondOfDay();
        int historyItemSeconds = lastItem.getDate().toLocalTime().toSecondOfDay();
        assertEquals(nowSeconds, historyItemSeconds, 5);

    }

    @Test
    public void testCancellingOrder () {
        //Using these StockItems:
        //StockItem {1L, "Lays chips", "Potato chips", 11.0, 5}
        //StockItem {2L, "Chupa-chups", "Sweets", 8.0, 8}
        //StockItem {3L, "Frankfurters", "Beer sauseges", 15.0, 12}
        //StockItem {4L, "Free Beer", "Student's delight", 0.0, 100}

        SoldItem soldChips = new SoldItem(dao.findStockItem(1), 1);
        SoldItem soldChups = new SoldItem(dao.findStockItem(2), 1);
        SoldItem soldFrank = new SoldItem(dao.findStockItem(3), 1);
        SoldItem soldBeer = new SoldItem(dao.findStockItem(4), 1);

        cart.addItem(soldChips);
        cart.addItem(soldChups);
        cart.cancelCurrentPurchase();

        cart.addItem(soldFrank);
        cart.addItem(soldBeer);
        cart.submitCurrentPurchase();

        List<HistoryItem> hil = dao.findAllHistoryItems();
        List<SoldItem> sil = hil.get(hil.size() - 1).getSoldItems(); //the SoldItems list from the last HistoryItem

        boolean consistsOfTwoItems = sil.size() == 2;
        boolean containsChips = sil.contains(soldChips);
        boolean containsChups = sil.contains(soldChups);
        boolean containsFrankFurters = sil.contains(soldFrank);
        boolean containsBeer = sil.contains(soldBeer);

        assertTrue(consistsOfTwoItems && !containsChips && !containsChups && containsFrankFurters && containsBeer);
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged () {
        //Using these StockItems:
        //StockItem {1L, "Lays chips", "Potato chips", 11.0, 5}
        //StockItem {2L, "Chupa-chups", "Sweets", 8.0, 8}
        StockItem chips = dao.findStockItem(1);
        StockItem chups = dao.findStockItem(2);
        int chipsQuantityBefore = chips.getQuantityInStock();
        int chupsQuantityBefore = chups.getQuantityInStock();
        cart.addItem(new SoldItem(chips, 3));
        cart.addItem(new SoldItem(chups, 3));
        cart.cancelCurrentPurchase();
        int chipsQuantityAfter = chips.getQuantityInStock();
        int chupsQuantityAfter = chups.getQuantityInStock();
        boolean chipsSameQuantity = chipsQuantityAfter == chipsQuantityBefore;
        boolean chupsSameQuantity = chupsQuantityAfter == chupsQuantityBefore;
        assertTrue(chipsSameQuantity && chupsSameQuantity);

    }

}

