import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class TestWarehouse {

    InMemorySalesSystemDAO dao;
    Warehouse warehouse;

    @Before
    public void setUp() {
        /** THESE ITEMS ARE ADDED IN CONSTRUCTOR WHEN CREATING THE InMemorySalesSystemDAO
         * items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
         * items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
         * items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
         * items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
         */
        dao = new InMemorySalesSystemDAO();
        warehouse = new Warehouse(dao);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        warehouse.addToStock(10L, "onion", "vegetables", 0.5, 10);
        List<String> calledMethods = dao.getMethodsCalled();
        boolean firstBegin = calledMethods.get(0).equals("beginTransaction");
        boolean thenCommit = calledMethods.get(1).equals("commitTransaction");
        boolean bothOnlyOnce = calledMethods.size() == 2;
        assertTrue(firstBegin && thenCommit && bothOnlyOnce);
    }

    @Test
    public void testAddingNewitem() {
        int size = dao.findStockItems().size();
        System.out.println("Before:");
        System.out.println(dao.findStockItems());
        warehouse.addToStock(10L, "potato", "", 5.0, 1);
        int size2 = dao.findStockItems().size();
        System.out.println("After: " + dao.findStockItems());
        assertNotEquals(size, size2);
    }

    @Test
    public void testAddingExistingItem() {
        //adding an item to test it after adding it again
        warehouse.addToStock(6L, "tomato", "", 5.0, 1);
        int quantity_before = dao.findStockItem(6L).getQuantityInStock();
        System.out.println("Quantity before: " + quantity_before);
        warehouse.addToStock(6L, "tomato", "", 5.0, 1);
        int quantity_after = dao.findStockItem(6L).getQuantityInStock();
        System.out.println("Quantity after: " + quantity_after);
        assertTrue(quantity_before != quantity_after);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddingItemWithNegativeQuantity() {
        warehouse.addToStock(14L, "carrot", "", 4.0, -1);
    }
}